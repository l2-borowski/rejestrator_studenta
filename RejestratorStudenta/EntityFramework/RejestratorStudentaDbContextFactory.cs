﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace RejestratorStudenta.EntityFramework
{
    public class RejestratorStudentaDbContextFactory : IDesignTimeDbContextFactory<RejestratorStudentaDbContext>
    {
        public RejestratorStudentaDbContext CreateDbContext(string[] args = null)
        {
            var options = new DbContextOptionsBuilder<RejestratorStudentaDbContext>();
            options.UseSqlite("Data Source=RejestratorStudentaDb.sqlite");

            return new RejestratorStudentaDbContext(options.Options);
        }
    }
}
