﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services
{
    public class GetTeachersService : IGetTeachersService
    {
        public GetTeachersService(IDataService<Teacher> teacherService)
        {
            this.teacherService = teacherService;
        }

        /// <summary>
        /// Returns collection of all teachers, from the database
        /// </summary>
        public async Task<IEnumerable<Teacher>> GetTeachers()
        {
            try
            {
                // Get all teachers from the database
                return await teacherService.GetAll();
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private readonly IDataService<Teacher> teacherService;
    }
}
