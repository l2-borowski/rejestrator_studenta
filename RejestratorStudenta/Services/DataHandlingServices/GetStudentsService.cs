﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services
{
    public class GetStudentsService : IGetStudentsService
    {
        public GetStudentsService(IDataService<Student> studentService)
        {
            this.studentService = studentService;        
        }

        /// <summary>
        /// Returns collection of all students, from the database
        /// </summary>
        public async Task<IEnumerable<Student>> GetStudents()
        {
            try
            {
                // Get all students from the database
                return await studentService.GetAll();
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private readonly IDataService<Student> studentService;
    }
}
