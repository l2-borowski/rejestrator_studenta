﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services
{
    public class AddStudentService : IAddStudentService
    {
        public AddStudentService(IDataService<Student> studentService)
        {
            this.studentService = studentService;
        }

        /// <summary>
        /// Validates new student object and displays appriopriate dialog box. If the validation
        /// is succesfully passed, new student is added to the database.
        /// </summary>
        public async Task<Student> AddStudent(Student student)
        {
            // Handle exception errors
            try
            {
                // Check if all the fields are filled in
                if (string.IsNullOrEmpty(student.FirstName))
                    throw new Exception("Podaj proszę imię studenta!");
                else if (string.IsNullOrEmpty(student.LastName))
                    throw new Exception("Podaj proszę nazwisko studenta!");
                else if (string.IsNullOrEmpty(student.PESEL))
                    throw new Exception("Podaj proszę PESEL studenta!");
                else if (student.DateOfBirth == DateTime.MinValue)
                    throw new Exception("Podaj proszę date urodzenia studenta!");

                // Check if student with the same PESEL already exists in the database
                IEnumerable<Student> students = studentService.GetAll().Result;
                if (students != null)
                {
                    if (students.Any(x => x.PESEL == student.PESEL))
                        throw new Exception("Student z tym samym numerem PESEL, już instnieję w bazie danych!");
                }

                // Create new student in the database
                await studentService.Create(student);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            // Display information dialog box with successfully added student message result
            MessageBox.Show($"{student.FullName} został dodany do bazy danych", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);

            return student;
        }

        private readonly IDataService<Student> studentService;
    }
}
