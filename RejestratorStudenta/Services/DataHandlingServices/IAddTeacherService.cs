﻿using RejestratorStudenta.Model;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IAddTeacherService
    {
        Task<Teacher> AddTeacher(Teacher teacher);
    }
}
