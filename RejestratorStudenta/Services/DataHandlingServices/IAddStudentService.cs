﻿using RejestratorStudenta.Model;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IAddStudentService
    {
        Task<Student> AddStudent(Student student);
    }
}
