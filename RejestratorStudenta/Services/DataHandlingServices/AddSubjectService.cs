﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services
{
    public class AddSubjectService : IAddSubjectService
    {
        public AddSubjectService(IDataService<Subject> subjectService)
        {
            this.subjectService = subjectService;
        }

        /// <summary>
        /// Validates new subject object and displays appriopriate dialog box. If the validation
        /// is succesfully passed, new subject is added to the database.
        /// </summary>
        public async Task<Subject> AddSubject(Subject subject)
        {
            // Handle exception errors
            try
            {
                // Check if all the fields are filled in
                if (string.IsNullOrEmpty(subject.Name))
                    throw new Exception("Podaj proszę nazwę przedmiotu!");
                else if (subject.Term == null)
                    throw new Exception("Wybierz proszę semestr studiów dla przedmiotu!");
                else if (subject.Teacher == null)
                    throw new Exception("Wybierz proszę prowadzącego dla przedmiotu!");

                // Check if subject with the same name already exists in the database
                IEnumerable<Subject> subjects = subjectService.GetAll().Result;
                if (subjects != null)
                {
                    if (subjects.Any(x => x.Name == subject.Name))
                    {
                        // Throw an exception and handle the error message in a view model
                        throw new ArgumentException("Subject with the same name already exists in the database.");
                    }
                }

                // Create new subject in the database
                await subjectService.Create(subject);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            // Display information dialog box with successfully added subject message result
            MessageBox.Show($"Dodano przedmiot: {subject.Name} do bazy danych", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);

            return subject;
        }

        private readonly IDataService<Subject> subjectService;
    }
}
