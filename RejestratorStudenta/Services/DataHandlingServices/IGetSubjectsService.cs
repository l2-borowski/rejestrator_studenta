﻿using RejestratorStudenta.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IGetSubjectsService
    {
        Task<IEnumerable<Subject>> GetSubjects();
    }
}
