﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services
{
    public class AddTeacherService : IAddTeacherService
    {
        public AddTeacherService(IDataService<Teacher> teacherService)
        {
            this.teacherService = teacherService;
        }

        /// <summary>
        /// Validates new teacher object and displays appriopriate dialog box. If the validation
        /// is succesfully passed, new teacher is added to the database.
        /// </summary>
        public async Task<Teacher> AddTeacher(Teacher teacher)
        {
            // Handle exception errors
            try
            {
                // Check if all the fields are filled in
                if (string.IsNullOrEmpty(teacher.FirstName))
                    throw new Exception("Podaj proszę imię prowadzącego!");
                else if (string.IsNullOrEmpty(teacher.LastName))
                    throw new Exception("Podaj proszę nazwisko Prowadzącego!");

                // Check if teacher with the same full name already exists in the database
                IEnumerable<Teacher> teachers = teacherService.GetAll().Result;
                if (teachers != null)
                {
                    if (teachers.Any(x => x.FullName == teacher.FullName))
                    {
                        // Throw an exception and handle the error message in a view model
                        throw new Exception("Prowadzący z tym samym imieniem i nazwiskiem, już istnieję w bazie danych!");
                    }
                }

                // Create new teacher in the database
                await teacherService.Create(teacher);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            // Display information dialog box with successfully added teacher message result
            MessageBox.Show($"{teacher.FullName} został dodany do bazy danych", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);

            return teacher;
        }

        private readonly IDataService<Teacher> teacherService;
    }
}
