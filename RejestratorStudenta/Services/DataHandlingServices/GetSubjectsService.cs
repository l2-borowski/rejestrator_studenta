﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services
{
    public class GetSubjectsService : IGetSubjectsService
    {
        public GetSubjectsService(IDataService<Subject> subjectService)
        {
            this.subjectService = subjectService;
        }

        /// <summary>
        /// Returns collection of all teachers, from the database
        /// </summary>
        public async Task<IEnumerable<Subject>> GetSubjects()
        {
            try
            {
                // Get all subjects from the database
                return await subjectService.GetAll();
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        private readonly IDataService<Subject> subjectService;
    }
}
