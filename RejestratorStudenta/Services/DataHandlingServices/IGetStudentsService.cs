﻿using RejestratorStudenta.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IGetStudentsService
    {
        Task<IEnumerable<Student>> GetStudents();
    }
}
