﻿using RejestratorStudenta.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IGetTeachersService
    {
        Task<IEnumerable<Teacher>> GetTeachers();
    }
}
