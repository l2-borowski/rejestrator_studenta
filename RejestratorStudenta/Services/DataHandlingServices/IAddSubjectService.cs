﻿using RejestratorStudenta.Model;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IAddSubjectService
    {
        Task<Subject> AddSubject(Subject subject);
    }
}
