﻿using RejestratorStudenta.Model;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services.RegisterServices
{
    public interface IUnregisterStudentService
    {
        Task<Subject> UnregisterStudent(Student student, Subject subject);
    }
}
