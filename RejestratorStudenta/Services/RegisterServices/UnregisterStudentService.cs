﻿using RejestratorStudenta.Model;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services.RegisterServices
{
    public class UnregisterStudentService : IUnregisterStudentService
    {
        public UnregisterStudentService(IDataService<Subject> subjectService)
        {
            this.subjectService = subjectService;
        }

        /// <summary>
        /// Unregisters student from a specified subject
        /// </summary>
        public async Task<Subject> UnregisterStudent(Student student, Subject subject)
        {
            // Handle exception errors
            try
            {
                // Check if selected subject has the requested student registered
                if (!subject.Students.Contains(student))
                    throw new Exception("Ten student jest niezarejestrowany, dla wybranego przedmiotu!");

                // Remove student from a subject and update the database
                subject.Students.Remove(student);
                await subjectService.Update(subject.Id, subject);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            return subject;
        }

        private readonly IDataService<Subject> subjectService;
    }
}
