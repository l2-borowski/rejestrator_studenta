﻿using RejestratorStudenta.Model;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IRegisterStudentService
    {
        Task<Subject> RegisterStudent(Student student, Subject subject);
    }
}
