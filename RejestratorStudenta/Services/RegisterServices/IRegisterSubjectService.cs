﻿using RejestratorStudenta.Model;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public interface IRegisterSubjectService
    {
        Task<Student> RegisterSubject(Subject subject, Student student);
    }
}
