﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services.RegisterServices
{
    public class UnregisterSubjectService : IUnregisterSubjectService
    {
        public UnregisterSubjectService(IDataService<Student> studentService)
        {
            this.studentService = studentService;
        }

        public async Task<Student> UnregisterSubject(Subject subject, Student student)
        {
            // Handle exception errors
            try
            {
                // Check if selected student has the requested subject registered
                if (!student.Subjects.Contains(subject))
                    throw new Exception("Ten przedmiot jest niezarejestrowany, dla wybranego studenta!");

                // Remove subject from a student and update the database
                student.Subjects.Remove(subject);
                await studentService.Update(student.Id, student);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            return student;
        }

        private readonly IDataService<Student> studentService;
    }
}
