﻿using RejestratorStudenta.Model;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services.RegisterServices
{
    public class RegisterStudentService : IRegisterStudentService
    {
        public RegisterStudentService(IDataService<Subject> subjectService)
        {
            this.subjectService = subjectService;
        }

        /// <summary>
        /// Registers student for a specified subject
        /// </summary>
        public async Task<Subject> RegisterStudent(Student student, Subject subject)
        {
            // Handle exception errors
            try
            {
                // Check if selected subject already has the requested student registered
                if (subject.Students.Contains(student))
                    throw new Exception("Ten student jest już zarejestrowany, dla wybranego przedmiotu!");

                // Add student for a subject and update the database
                subject.Students.Add(student);
                await subjectService.Update(subject.Id, subject);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            return subject;
        }

        private readonly IDataService<Subject> subjectService;
    }
}
