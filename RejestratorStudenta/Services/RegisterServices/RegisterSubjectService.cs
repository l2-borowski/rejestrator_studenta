﻿using RejestratorStudenta.Model;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace RejestratorStudenta.Services.RegisterServices
{
    public class RegisterSubjectService : IRegisterSubjectService
    {
        public RegisterSubjectService(IDataService<Student> studentService)
        {
            this.studentService = studentService;
        }

        /// <summary>
        /// Registers subject for a specified student.
        /// </summary>
        public async Task<Student> RegisterSubject(Subject subject, Student student)
        {
            // Handle exception errors
            try
            {
                // Check if selected student already has the requested subject registered
                if (student.Subjects.Contains(subject))
                    throw new Exception("Ten przedmiot jest już zarejestrowany, dla wybranego studenta!");

                // Add subject for a student and update the database
                student.Subjects.Add(subject);
                await studentService.Update(student.Id, student);
            }
            catch (Exception e)
            {
                // Display error dialog box with an exception error message and return
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            return student;
        }

        private readonly IDataService<Student> studentService;
    }
}
