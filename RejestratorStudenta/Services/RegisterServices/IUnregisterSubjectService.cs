﻿using RejestratorStudenta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services.RegisterServices
{
    public interface IUnregisterSubjectService
    {
        Task<Student> UnregisterSubject(Subject subject, Student student);
    }
}
