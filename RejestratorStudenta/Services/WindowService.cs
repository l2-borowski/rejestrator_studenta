﻿using System.Windows;

public class WindowService : IWindowService
{
    /// <summary>
    /// Opens new window from provided view model
    /// </summary>
    public void OpenNewWindow(object viewModel)
    {
        var window = new Window();
        window.Content = viewModel;
        window.SizeToContent = SizeToContent.WidthAndHeight;
        window.ResizeMode = ResizeMode.NoResize;
        window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        window.ShowDialog();
    }

}