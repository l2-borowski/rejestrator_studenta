﻿public interface IWindowService
{
    void OpenNewWindow(object viewModel);
}