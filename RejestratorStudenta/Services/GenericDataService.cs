﻿using Microsoft.EntityFrameworkCore;
using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RejestratorStudenta.Services
{
    public class GenericDataService<T> : IDataService<T> where T : DomainObject
    {
        public GenericDataService(RejestratorStudentaDbContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;

            // Create new database file if one doesn't exist
            using (RejestratorStudentaDbContext context = contextFactory.CreateDbContext())
            {
                context.Database.EnsureCreated();
            }
        }

        public async Task<T> Create(T entity)
        {
            using (RejestratorStudentaDbContext context = contextFactory.CreateDbContext())
            {
                var createdResult = await context.Set<T>().AddAsync(entity);
                await context.SaveChangesAsync();

                return createdResult.Entity;
            }
        }

        public async Task<bool> Delete(int id)
        {
            using (RejestratorStudentaDbContext context = contextFactory.CreateDbContext())
            {
                T entity = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                context.Set<T>().Remove(entity);
                await context.SaveChangesAsync();

                return true;
            }
        }

        public async Task<T> Get(int id)
        {
            using (RejestratorStudentaDbContext context = contextFactory.CreateDbContext())
            {
                T entity = await context.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);

                return entity;
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            using (RejestratorStudentaDbContext context = contextFactory.CreateDbContext())
            {
                IEnumerable<T> entities = await context.Set<T>().ToListAsync();

                return entities;
            }
        }

        public async Task<T> Update(int id, T entity)
        {
            using (RejestratorStudentaDbContext context = contextFactory.CreateDbContext())
            {
                entity.Id = id;

                context.Set<T>().Update(entity);
                await context.SaveChangesAsync();

                return entity;
            }
        }

        private readonly RejestratorStudentaDbContextFactory contextFactory;
    }
}
