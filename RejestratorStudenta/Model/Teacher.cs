﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RejestratorStudenta.Model
{
    public class Teacher : DomainObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }
        
        public ICollection<Subject> Subjects { get; set; }
    }
}
