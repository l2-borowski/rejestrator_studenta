﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RejestratorStudenta.Model
{
    public enum Term
    {
        Winter, 
        Summer
    }

    public class Subject : DomainObject
    {
        public string Name { get; set; }
        public Term? Term { get; set; }
        
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public ICollection<Student> Students { get; set; } = new List<Student>();
    }
}
