﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RejestratorStudenta.Model
{
    public class Student : DomainObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PESEL { get; set; }
        public DateTime DateOfBirth { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }

        public ICollection<Subject> Subjects { get; set; } = new List<Subject>();
    }
}
