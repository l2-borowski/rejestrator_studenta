﻿public interface IDataPanel
{
    void GetStudents();
    void GetSubjects();
}