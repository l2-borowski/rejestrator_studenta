﻿using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using System.Collections.Generic;

namespace RejestratorStudenta.Interfaces
{
    public class DataPanel : ObservableObject, IDataPanel
    {
        public Student SelectedStudent
        {
            get { return selectedStudent; }
            set
            {
                selectedStudent = value;
                OnPropertyChanged(nameof(SelectedStudent));
            }
        }

        public Subject SelectedSubject
        {
            get { return selectedSubject; }
            set
            {
                selectedSubject = value;
                OnPropertyChanged(nameof(SelectedSubject));
            }
        }

        public IEnumerable<Student> Students 
        {
            get { return students; } 
            set
            {
                students = value;
                OnPropertyChanged(nameof(Students));
            }
        }

        public IEnumerable<Subject> Subjects
        {
            get { return subjects; }
            set
            {
                subjects = value;
                OnPropertyChanged(nameof(Subject));
            }
        }

        public DataPanel()
        {
            GetStudents();
            GetSubjects();
        }

        public async void GetStudents()
        {
            IDataService<Student> studentService =
                new GenericDataService<Student>(
                    new RejestratorStudentaDbContextFactory()
                );

            IGetStudentsService getStudentsService = new GetStudentsService(studentService);
            Students = await getStudentsService.GetStudents();
        }

        public async void GetSubjects()
        {
            IDataService<Subject> subjectService =
                new GenericDataService<Subject>(
                    new RejestratorStudentaDbContextFactory()
                );

            IGetSubjectsService getSubjectsService = new GetSubjectsService(subjectService);
            Subjects = await getSubjectsService.GetSubjects();
        }

        private Student selectedStudent;
        private Subject selectedSubject;
        private IEnumerable<Student> students;
        private IEnumerable<Subject> subjects;
    }
}
