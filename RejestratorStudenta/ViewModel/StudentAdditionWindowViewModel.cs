﻿using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using System;
using System.Windows;
using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class StudentAdditionWindowViewModel
    {
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public string StudentPESEL { get; set; }
        public DateTime StudentDateOfBirth { get; set; }

        public ICommand AddNewStudentCommand { get; set; }
        public ICommand CloseDialogWindowCommand { get; set; }

        public StudentAdditionWindowViewModel()
        {
            AddNewStudentCommand = new RelayCommand(() => AddNewStudent());
            CloseDialogWindowCommand = new RelayCommand(() => CloseDialogWindow());
        }

        public async void AddNewStudent()
        {
            IDataService<Student> studentService =
                new GenericDataService<Student>(
                    new RejestratorStudentaDbContextFactory()
                );

            IAddStudentService addStudentService = new AddStudentService(studentService);

            var newStudent =
                new Student{
                    FirstName = StudentFirstName,
                    LastName = StudentLastName,
                    PESEL = StudentPESEL,
                    DateOfBirth = StudentDateOfBirth
                };

            var student = await addStudentService.AddStudent(newStudent);
            if (student != null)
                CloseDialogWindow();
        }

        private void CloseDialogWindow()
        {
            //TODO: Get reference to new window and then close it - This is a temporary solution
            Application.Current.Windows[1].Close();
        }
    }
}
