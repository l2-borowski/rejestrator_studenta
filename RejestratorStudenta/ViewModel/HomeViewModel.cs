﻿using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class HomeViewModel : ObservableObject
    {
        public StudentControlViewModel StudentControlVm { get; set; }
        public SubjectControlViewModel SubjectControlVm { get; set; }

        public ICommand StudentControlCommand { get; set; }
        public ICommand SubjectControlCommand { get; set; }

        public object CurrentView
        {
            get { return currentView; }
            set
            {
                currentView = value;
                OnPropertyChanged(nameof(CurrentView));
            }
        }

        public HomeViewModel()
        {
            StudentControlVm = new StudentControlViewModel();
            SubjectControlVm = new SubjectControlViewModel();

            StudentControlCommand = new RelayCommand(() => ShowStudentControlView());
            SubjectControlCommand = new RelayCommand(() => ShowSubjectControlview());

            ShowStudentControlView();
        }

        public void ShowStudentControlView()
        {
            CurrentView = StudentControlVm;
        }

        public void ShowSubjectControlview()
        {
            CurrentView = SubjectControlVm;
        }

        private object currentView;
    }
}
