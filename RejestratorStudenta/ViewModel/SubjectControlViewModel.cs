﻿using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Interfaces;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using RejestratorStudenta.Services.RegisterServices;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class SubjectControlViewModel : DataPanel
    {
        public List<Subject> StudentSubjects { get; set; }
        public ICommand RegisterStudentForSubjectCommand { get; set; }
        public ICommand UnregisterStudentForSubjectCommand { get; set; }

        public SubjectControlViewModel()
        {
            RegisterStudentForSubjectCommand = new RelayCommand(() => RegisterStudentForSubject());
            UnregisterStudentForSubjectCommand = new RelayCommand(() => UnegisterStudentForSubject());
        }

        public async void RegisterStudentForSubject()
        {
            if (SelectedStudent == null)
            {
                MessageBox.Show("Student not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (SelectedSubject == null)
            {
                MessageBox.Show("Subject not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IDataService<Subject> subjectService =
                new GenericDataService<Subject>(
                    new RejestratorStudentaDbContextFactory()
                );

            IRegisterStudentService registerStudentService = new RegisterStudentService(subjectService);

            SelectedSubject =
                await registerStudentService.RegisterStudent(SelectedStudent, SelectedSubject);
        }

        public async void UnegisterStudentForSubject()
        {
            if (SelectedStudent == null)
            {
                MessageBox.Show("Student not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (SelectedSubject == null)
            {
                MessageBox.Show("Subject not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IDataService<Subject> subjectService =
                new GenericDataService<Subject>(
                    new RejestratorStudentaDbContextFactory()
                );

            IUnregisterStudentService unregisterStudentService = new UnregisterStudentService(subjectService);

            SelectedSubject =
                await unregisterStudentService.UnregisterStudent(SelectedStudent, SelectedSubject);
        }
    }
}
