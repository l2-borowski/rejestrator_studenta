﻿using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using System.Windows;
using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class TeacherAdditionWindowViewModel
    {
        public string TeacherFirstName { get; set; }
        public string TeacherLastName { get; set; }

        public ICommand AddNewTeacherCommand { get; set; }
        public ICommand CloseDialogWindowCommand { get; set; }

        public TeacherAdditionWindowViewModel()
        {
            AddNewTeacherCommand = new RelayCommand(() => AddNewTeacher());
            CloseDialogWindowCommand = new RelayCommand(() => CloseDialogWindow());
        }

        public async void AddNewTeacher()
        {
            IDataService<Teacher> teacherService =
                new GenericDataService<Teacher>(
                    new RejestratorStudentaDbContextFactory()
                );

            IAddTeacherService addTeacherService = new AddTeacherService(teacherService);

            var newTeacher =
                new Teacher{
                    FirstName = TeacherFirstName,
                    LastName = TeacherLastName
                };

            var teacher = await addTeacherService.AddTeacher(newTeacher);
            if (teacher != null) 
                CloseDialogWindow();
        }

        private void CloseDialogWindow()
        {
            //TODO: Get reference to new window and then close it - This is a temporary solution
            Application.Current.Windows[1].Close();
        }
    }
}
