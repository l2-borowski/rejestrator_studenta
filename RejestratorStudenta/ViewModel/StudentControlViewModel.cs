﻿using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Interfaces;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using RejestratorStudenta.Services.RegisterServices;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class StudentControlViewModel :  DataPanel
    {
        public List<Subject> StudentSubjects { get; set; }
        public ICommand RegisterSubjectForStudentCommand { get; set; }
        public ICommand UnregisterSubjectForStudentCommand { get; set; }

        public StudentControlViewModel()
        {
            RegisterSubjectForStudentCommand = new RelayCommand(() => RegisterSubjectForStudent());
            UnregisterSubjectForStudentCommand = new RelayCommand(() => UnegisterSubjectForStudent());
         }

        public async void RegisterSubjectForStudent()
        {
            if (SelectedStudent == null)
            {
                MessageBox.Show("Student not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (SelectedSubject == null)
            {
                MessageBox.Show("Subject not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IDataService<Student> studentService =
                new GenericDataService<Student>(
                    new RejestratorStudentaDbContextFactory()
                );

            IRegisterSubjectService registerSubjectService = new RegisterSubjectService(studentService);

            SelectedStudent =
                await registerSubjectService.RegisterSubject(SelectedSubject, SelectedStudent);
        }

        public async void UnegisterSubjectForStudent()
        {
            if (SelectedStudent == null)
            {
                MessageBox.Show("Student not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (SelectedSubject == null)
            {
                MessageBox.Show("Subject not selected!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IDataService<Student> studentService =
                new GenericDataService<Student>(
                    new RejestratorStudentaDbContextFactory()
                );

            IUnregisterSubjectService unregisterSubjectService = new UnregisterSubjectService(studentService);

            SelectedStudent =
                await unregisterSubjectService.UnregisterSubject(SelectedSubject, SelectedStudent);
        }
    }
}
