﻿using System;
using System.Windows.Input;

public class RelayCommand : ICommand
{
    public event EventHandler CanExecuteChanged = (sender, e) => { };

    public RelayCommand(Action action)
    {
        this.action = action;
    }

    public bool CanExecute(object parameter)
    {
        return true;
    }

    public void Execute(object parameter)
    {
        action();
    }

    private Action action;

    //public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
    //{
    //    this.execute = execute;
    //    this.canExecute = canExecute;
    //}

    //public event EventHandler CanExecuteChanged
    //{
    //    add { CommandManager.RequerySuggested += value; }
    //    remove { CommandManager.RequerySuggested -= value; }
    //}

    //public bool CanExecute(object parameter)
    //{
    //    return this.canExecute == null || this.canExecute(parameter);
    //}

    //public void Execute(object parameter)
    //{
    //    this.execute(parameter);
    //}

    //private Action<object> execute;
    //private Func<object, bool> canExecute;
}