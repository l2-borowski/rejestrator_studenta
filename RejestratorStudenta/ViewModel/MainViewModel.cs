﻿using System.Windows;
using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class MainViewModel : ObservableObject
    {
        public HomeViewModel HomeVm { get; set; }
        public StudentAdditionWindowViewModel StudentAdditionVm { get; set; }
        public TeacherAdditionWindowViewModel TeacherAdditionVm { get; set; }
        public SubjectAdditionWindowViewModel SubjectAdditionVm { get; set; }

        public ICommand StudentAdditionWindowCommand { get; set; }
        public ICommand TeacherAdditionWindowCommand { get; set; }
        public ICommand SubjectAdditionWindowCommand { get; set; }
        public ICommand ExitApplicationCommand { get; set; }

        public object CurrentView
        {
            get { return currentView; }
            set
            {
                currentView = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            windowService = new WindowService();

            HomeVm = new HomeViewModel();
            StudentAdditionVm = new StudentAdditionWindowViewModel();
            TeacherAdditionVm = new TeacherAdditionWindowViewModel();
            SubjectAdditionVm = new SubjectAdditionWindowViewModel();

            // Create commands
            StudentAdditionWindowCommand = new RelayCommand(() => windowService.OpenNewWindow(StudentAdditionVm));
            TeacherAdditionWindowCommand = new RelayCommand(() => windowService.OpenNewWindow(TeacherAdditionVm));
            SubjectAdditionWindowCommand = new RelayCommand(() => windowService.OpenNewWindow(SubjectAdditionVm));
            ExitApplicationCommand = new RelayCommand(() => ExitApplication());

            CurrentView = HomeVm;
        }

        private void ExitApplication()
        {
            Application.Current.Shutdown();
        }

        private object currentView;
        private WindowService windowService;
    }
}
