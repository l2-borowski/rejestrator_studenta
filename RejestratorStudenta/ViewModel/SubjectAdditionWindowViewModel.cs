﻿using RejestratorStudenta.EntityFramework;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace RejestratorStudenta.ViewModel
{
    public class SubjectAdditionWindowViewModel
    {
        public string SubjectName { get; set; }
        public Term SubjectTerm { get; set; }
        public IEnumerable<Teacher> Teachers { get; set; }
        public Teacher SelectedSubjectTeacher { get; set; }

        public ICommand AddNewSubjectCommand { get; set; }
        public ICommand CloseDialogWindowCommand { get; set; }

        public SubjectAdditionWindowViewModel()
        {
            GetTeachers();

            AddNewSubjectCommand = new RelayCommand(() => AddNewSubject());
            CloseDialogWindowCommand = new RelayCommand(() => CloseDialogWindow());
        }

        public async void AddNewSubject()
        {
            IDataService<Subject> subjectService =
                new GenericDataService<Subject>(
                    new RejestratorStudentaDbContextFactory()
                );

            IAddSubjectService addSubjectService = new AddSubjectService(subjectService);

            var newSubject =
                new Subject{
                    Name = SubjectName,
                    Term = SubjectTerm,
                    Teacher = SelectedSubjectTeacher
                };

            var subject = await addSubjectService.AddSubject(newSubject);
            if (subject != null) 
                CloseDialogWindow();
        }

        private async void GetTeachers()
        {
            IDataService<Teacher> teacherService =
                new GenericDataService<Teacher>(
                    new RejestratorStudentaDbContextFactory()
                );

            IGetTeachersService getTeachersService = new GetTeachersService(teacherService);
            Teachers = await getTeachersService.GetTeachers();
        }

        private void CloseDialogWindow()
        {
            //TODO: Get reference to new window and then close it - This is a temporary solution
            Application.Current.Windows[1].Close();
        }
    }
}
