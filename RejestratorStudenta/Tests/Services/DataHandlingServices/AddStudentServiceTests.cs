﻿using Moq;
using NUnit.Framework;
using RejestratorStudenta.Model;
using RejestratorStudenta.Services;
using System;

namespace RejestratorStudenta.Tests.Services.DataHandlingServices
{
    [TestFixture]
    public class AddStudentServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            mockStudentService = new Mock<IDataService<Student>>();
            addStudentService = new AddStudentService(mockStudentService.Object);
        }

        [Test]
        public void AddStudent_WithoutProvidingFirstName_ThrowsException()
        {
            var student =
                new Student
                {
                    LastName = "LastNameTest",
                    PESEL = "412412412",
                    DateOfBirth = new DateTime(1993, 10, 5)
                };

            Assert.ThrowsAsync<Exception>(() => addStudentService.AddStudent(student));
        }

        [Test]
        public void AddStudent_WithoutProvidingLastName_ThrowsException()
        {
            var student =
                new Student
                {
                    FirstName = "FirstNameTest",
                    PESEL = "412412412",
                    DateOfBirth = new DateTime(1993, 10, 5)
                };

            Assert.ThrowsAsync<Exception>(() => addStudentService.AddStudent(student));
        }

        [Test]
        public void AddStudent_WithoutProvidingPESEL_ThrowsException()
        {
            var student =
                new Student
                {
                    FirstName = "FirstNameTest",
                    LastName = "LastNameTest",
                    DateOfBirth = new DateTime(1993, 10, 5)
                };

            Assert.ThrowsAsync<Exception>(() => addStudentService.AddStudent(student));
        }

        private AddStudentService addStudentService;
        private Mock<IDataService<Student>> mockStudentService;

    }
}
